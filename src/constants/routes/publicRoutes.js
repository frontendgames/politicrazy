import  {Home} from '../../views/public/home';
import Error from "../../views/public/error";
export const publicRoutes = {

    Home: {
        component: Home,
        path: '/'
    },
    Error:{
        component: Error,
        path: '/error'
    }
};
