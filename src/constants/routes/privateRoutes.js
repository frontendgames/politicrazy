import  {Home} from '../../views/public/home';
import {Party} from '../../views/private/party';
import {PartyList} from '../../views/private/partyList';
import {Country} from '../../views/private/country';
import {CountryList} from '../../views/private/countryList';
import {ElectionList} from '../../views/private/electionList';
import {Election} from '../../views/private/election';
import {Continent} from "../../views/private/continent";
import {Profile} from '../../views/private/profile';
export const privateRoutes = {
    Home: {
        component: Home,
        path: '/home'
    },
    CountryList: {
        component: CountryList,
        path: '/countries'
    },
    Country: {
        component: Country,
        path: '/countries/:idCountry'
    },
    Party: {
        component: Party,
        path: '/parties/:idParty'
    },
    Parties: {
        component: PartyList,
        path: '/parties'
    },
    Elections: {
        component: ElectionList,
        path: '/elections'
    },
    Election:{
        component: Election,
        path: '/elections/:idElection'
    },
    Profile: {
        component: Profile,
        path: '/profile'
    },
    Continents: {
        component: Continent,
        path: '/continent'
    },
};

