import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Loading from '../../views/public/loading/';
import { withRouter } from 'react-router-dom';
import { Redirect } from "react-router";
import privateRoutes from '../../constants/routes/privateRoutes';
import publicRoutes from '../../constants/routes/publicRoutes';

class Callback extends Component {


    constructor(props) {
        super(props);
        this.state = {
            pastDelay: true, redirect:false, error:''

        };

    }
    /*
    async componentDidMount() {
        let error;
        try {
           // console.log(this.props);
            const token= await this.props.auth.handleAuthentication();
            console.log("pasa "+token);
            localStorage.setItem('id_token',token);
        } catch (e) {
            console.error(e);
            error = e;
        }
        this.setState({pastDelay:false,redirect:true, error:error});
        // browserHistory.push('/')

    }
*/

    render() {
        const redirect = this.state.redirect;
        const error = this.state.error;
        console.log("Callback");
        console.log(error,redirect);

        if (redirect && !error) {
            return (<Redirect to={privateRoutes.Home.path}/>)
        } else if (redirect && error){
            return (<Redirect to={{ pathname: publicRoutes.Error.path,state: { message: error,code:'401' }}} />)

        }else {

            return (
                <React.Fragment>
                    Callback
                    <Loading error={null} pastDelay={this.state.pastDelay} timedOut={null}/>
                </React.Fragment>
            )
        }
    };
}
export default withRouter(Callback);

Callback.propTypes = {
    auth: PropTypes.object
}
