const AUTH_CONFIG_PROD = {
    domain: 'observer.eu.auth0.com',
    clientId: 'ceZjeQhNN4BUxASm49UDlQo2C4ZkZW6i',
    callbackUrl: 'https://politicrazy.herokuapp.com/'
};

const AUTH_CONFIG_DEVELOP = {
    domain: 'observer.eu.auth0.com',
    clientId: 'ceZjeQhNN4BUxASm49UDlQo2C4ZkZW6i',
    callbackUrl: 'http://localhost:3000/'
};


export const AUTH_CONFIG = process.env.REACT_APP_HOST === 'production' ? AUTH_CONFIG_PROD : AUTH_CONFIG_DEVELOP;