import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import "core-js/stable";
import "regenerator-runtime/runtime";
import React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
//import * as serviceWorker from './serviceWorker';
import { ApolloProvider, ApolloClient, InMemoryCache} from '@apollo/client';
import Template from './template';
import configureStore from './store/configureStore';
import { BrowserRouter } from "react-router-dom";





let ACCESS_TOKEN = localStorage.getItem('id_token');
const client = new ApolloClient({
    uri: 'https://politicazy.herokuapp.com/v1/graphql',
    cache: new InMemoryCache(),
    headers: {
        'Authorization': 'Bearer '+ACCESS_TOKEN,
        "content-type": "application/json"
}});

const PoliticrazyApp = () => (
    <ApolloProvider client={client}>
        <Provider store={configureStore()} >
            <BrowserRouter>
                <Template />
            </BrowserRouter>
        </Provider>
    </ApolloProvider>
);
ReactDOM.render(
    <PoliticrazyApp/>,
    document.getElementById('root')
);
//serviceWorker.register();

const mode = process.env.REACT_APP_HOST === 'production';


if (mode){
    console.log("Mode: "+mode);

    const express = require('express');
    const path = require('path');
    const app = express();
    const port = process.env.PORT || 8080; //Line 3


    // Serve static files from the React app
    const publicPath = path.join(__dirname, '..', 'public');
    app.use(express.static(publicPath));

    app.listen(port, () => {
        console.log(`Server is up on port ${port}!`);
    });
}
