import React, {Component} from "react";

export default class Footer extends Component {
    render() {
        return (
            <footer className='container-fluid App-footer'>
                <div>
                    <p>{new Date().getFullYear()} &copy;</p>
                </div>
            </footer>
        );
    }
}
