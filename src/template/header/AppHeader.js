import React, { Component } from 'react';
import Menu from "../menu/menu"
import * as Proptypes from "prop-types";
export default class Header extends Component {
    render() {
        const auth = this.props.auth ? this.props.auth:null;

        return (
            <header>
                <Menu auth={auth}/>
            </header>
        );
    }
}

Header.propTypes = {
    auth: Proptypes.object.isRequired
};
