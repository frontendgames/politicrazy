import React, { Component } from 'react';
import * as Proptypes from 'prop-types';
//import Header from '../header/AppHeader';
//import Footer from '../footer/AppFooter';

export class PublicLayout extends Component {


    render() {
        const Component = this.props.component;
      //  const auth = this.props.auth;
        return (
            <Component/>
        );
    }
}

PublicLayout.propTypes = {
    component: Proptypes.any.isRequired,
  //  route: Proptypes.object.isRequired,
    auth: Proptypes.object.isRequired
};