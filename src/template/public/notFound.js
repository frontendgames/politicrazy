import React, { Component } from 'react';
import {NotFoundComponent} from '../../views/public/notFound/index';

export default class NotFound extends Component {

    render() {
        return (
            <NotFoundComponent/>
        );
    }
}