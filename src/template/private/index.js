import React from 'react';
import * as Proptypes from "prop-types";
//import Header  from '../header/AppHeader';
//import Footer  from  '../footer/AppFooter';
//import { Redirect } from "react-router";
import Error from "../../views/public/error";


/*
export default class PrivateLayout extends Component {

    render() {
        const Component = this.props.component;
        const auth = this.props.auth;
        const { isAuthenticated} = this.props.auth;
        console.log(isAuthenticated());
        return (

            <React.Fragment>
                <Header auth={auth}/>
                <main className='container-fluid content'>
                    <Component auth={auth}/>
                </main>
                <Footer/>
            </React.Fragment>

        );
    }
}
*/

export const PrivateLayout = props => {
  //  const { Component, auth } = props;
    const Component = props.component;
    const auth = props.auth;
    const { isAuthenticated} = props.auth;
    console.log(isAuthenticated());
    if (!isAuthenticated()) return <Error code={'401'} message={'you need permission to access this page.'} />;

    return (

        <React.Fragment>
                <Component auth={auth}/>
        </React.Fragment>

    );
};


PrivateLayout.propTypes = {
    route: Proptypes.object.isRequired,
    auth: Proptypes.object.isRequired
};
