import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Switch, Redirect  } from 'react-router-dom';
import _ from 'lodash';
import {PrivateLayout} from './private';
import {PublicLayout}  from './public';
import history from '../components/auth/History';
import {privateRoutes} from '../constants/routes/privateRoutes';
import {publicRoutes}  from '../constants/routes/publicRoutes';
import {sessionRoutes} from '../constants/routes/sessionRoutes';
import * as Proptypes from "prop-types";
import NotFound from './public/notFound';
import {Home} from '../views/public/home/index';
import Auth from '../components/auth/Auth';
import Header from "./header/AppHeader";
import Footer from "./footer/AppFooter";
import Loading from "../views/public/loading";



class Template extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);

       // const auth = new Auth(this.props.dispatch);
        //this.state = {auth: auth};
        this.auth = new Auth(this.props.dispatch);
        this.state = {
            pastDelay: true, error:null

        };
    }
    async componentDidMount() {
        let error;
        this._isMounted = true;
        let token;
        //TODO migrarlo a Express con cookie HTTPOnly
        await this.auth.handleAuthentication();
        if(this._isMounted){
            token = this.auth.getIdToken();
         //   console.log(token);
        }
        try {

            if (token) {

                //TODO migrarlo a Express con cookie HTTPOnly
                localStorage.setItem('id_token', JSON.stringify(token));
                history.push(publicRoutes.Home.path);
            } else {
                const isLogged = localStorage.getItem('isLoggedIn');
                if(isLogged ==='true'){
                    await this.auth.renewSession();
                    const token = this.auth.getIdToken();
                 //   console.log(token);

                    if(token){
                        localStorage.setItem('id_token', JSON.stringify(token));
                    } else{
                        localStorage.removeItem('isLoggedIn');
                        localStorage.removeItem('id_token');
                    }
                }
            }
        } catch (e) {
            //TODO estructura de errores
            console.error(e);
            error = e;
        }

        this.setState({pastDelay: false, redirect: true, error: error});


    }
    render() {
        const auth = this.auth;
         if(this.state.pastDelay) {//loading
            return (
                <React.Fragment>
                    <Router history={history}>
                        <Header auth={auth}/>
                        <main className='container-fluid content'>
                            <Loading error={null} pastDelay={this.state.pastDelay} timedOut={null}/>
                        </main>
                        <Footer/>
                    </Router>
                </React.Fragment>
            )
        }
        const { isAuthenticated } = auth;
        return (
            <React.Fragment>
                <Router history={history}>
                    <Header auth={auth}/>
                    <main className='container-fluid content'>
                        <Switch>

                            { _.map(publicRoutes, (route, key) => {
                                const { component, path } = route;
                                return (
                                    <Route
                                        exact
                                        path={path}
                                        key={key}
                                        render={ (route) => <PublicLayout component={component} route={route} auth={this.auth}/>}
                                    />
                                )
                            }) }

                            { _.map(privateRoutes, (route, key) => {
                                const { component, path } = route;
                                // console.log(isAuthenticated());
                                return (

                                    <Route
                                        path={path}
                                        key={key}
                                        render={ (route) =>
                                            isAuthenticated()? (
                                                <PrivateLayout component={component} route={route}  auth={this.auth}/>
                                            ) : (
                                                <PublicLayout auth={this.auth} component={Home} route={route} />
                                            )
                                        }
                                    />
                                )
                            }) }

                            { _.map(sessionRoutes, (route, key) => {
                                const { component, path } = route;
                                return (
                                    <Route
                                        path={path}
                                        key={key}
                                        render={ (route) =>
                                            isAuthenticated() ? (
                                                <Redirect to="/"/>
                                            ) : (
                                                <PublicLayout auth={this.auth} component={component} route={route} />
                                            )
                                        }
                                    />
                                )
                            }) }

                            <Route component={ NotFound } />
                        </Switch>
                    </main>
                <Footer/>
            </Router>
            </React.Fragment>
        );
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
}
Template.propTypes = {
    Component: Proptypes.instanceOf(Component)

};
function mapStateToProps(state, props) { return { user: props } }
function mapDispatchToProps(dispatch) { return { dispatch }; }

export default  connect(
    mapStateToProps,
    mapDispatchToProps
)(Template);