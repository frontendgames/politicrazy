import React from 'react';
import { gql, useQuery } from '@apollo/client';

import Loading from '../../public/loading';

const GET_PARTY = gql`
  query Party($shortname: String) {
    party(shortname: $shortname) {
    description
    partylogo
    partyname
    shortname
    }
  }
`;
export function Party() {
    const { loading, error, data } = useQuery(GET_PARTY);

    console.log(error);
    if (loading) return <Loading/>;
    if (error) return <Loading error={error}/>;
    console.log(data);
    return (<PartyItem parties={data.party}/>);
}

const PartyItem = ({party})  => {
    console.log(party);
    return (<React.Fragment/>)
};

