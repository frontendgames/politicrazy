import React from 'react';
import { gql, useQuery } from '@apollo/client';
import Loading from '../../public/loading';
import {CardGroup,Card,CardBody,CardTitle,CardSubtitle,CardLink,CardText,CardImg } from 'reactstrap';
//import Party from "../party";

const GET_PARTIES_LIST = gql`
  query {
    party{
    description
    partyname
    shortname
    partylogo
    }
  }
`;
export function  PartyList() {
    const { loading, error, data } = useQuery(GET_PARTIES_LIST);

    console.log(error);
    if (loading) return <Loading/>;
    if (error) return <Loading error={error}/>;
    console.log(data);
    return <PartyListItem parties={data.party}/>;
}

 const PartyListItem = ({parties})  => {
    console.log(parties);
     return (<CardGroup>
         {parties.map((party,key) => {
             return (<Card key={key}>
                 <CardImg width="100%" src={party.partylogo}  alt="Card image cap" />
                 <CardBody>
                     <CardTitle>{party.shortname}</CardTitle>
                     <CardSubtitle>{party.partyname}</CardSubtitle>
                 </CardBody>

                 <CardBody>
                     <CardText>{party.description}</CardText>
                     <CardLink href={'/parties/'+party.shortname}>See Party</CardLink>
                 </CardBody>
             </Card>)
         })}
        </CardGroup>);
 };

