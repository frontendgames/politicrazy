import React from 'react';
import * as Proptypes from "prop-types";

export default class Error extends React.Component {
    render() {
        const error = this.props.message;
        return <div> Error:{error} </div>
    }
}

Error.propTypes = {
    message: Proptypes.string.isRequired,
    code: Proptypes.number.isRequired
};


